from django import forms
from django_registration.forms import RegistrationForm

from user.models import User


class UserRegistrationForm(RegistrationForm):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)

    class Meta(RegistrationForm.Meta):
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']
