let baseUrl = window.location.origin;

$(document).ready(function () {
    // Card Validation
    $("#id_card_num").on('change', (e) => {
        var ccNum = (document.getElementById("id_card_num").value).replace(/ /g, '');
        var isValid = false;
        const data = [/^(?:4[0-9]{12}(?:[0-9]{3})?)$/, /^(?:5[1-5][0-9]{14})$/, /^(?:3[47][0-9]{13})$/, /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/, /^389[0-9]{11}$/, /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/, /^(?:2131|1800|35\d{3})\d{11}$/, /^(62[0-9]{14,17})$/]
        for (let i = 0; i < data.length; i++) {
            if (data[i].test(ccNum) === true) {
                isValid = true;
                break;
            }
        }
        if (!isValid) {
            var card_num_error = "Please enter a valid card number."
            document.getElementById("card_num_error").innerHTML = card_num_error;
            $('#validateDetail').addClass('disable');
        } else {
            document.getElementById("card_num_error").innerHTML = "";
            $('#validateDetail').removeClass('disable');
        }

    })

    // Card formatting
    $('#id_card_num').on('keypress change', function () {
        $(this).val(function (index, value) {
            return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
        });
    });

});

function storeValue(product_id, price_id) {
    sessionStorage.setItem("product_id", product_id);
    sessionStorage.setItem("price_id", price_id);
    window.location = baseUrl + '/accounts/register/'
}


function createCustomer() {
    var firstName = document.getElementById('id_first_name').value;
    var lastName = document.getElementById('id_last_name').value;
    var billingAddress = document.getElementById('id_billing_address').value;
    var country = document.getElementById('id_country').value;
    var state = document.getElementById('id_state').value;
    var city = document.getElementById('id_city').value;
    var zip = document.getElementById('id_postal_code').value;
    var email = document.getElementById('id_email').value;

    var priceId = sessionStorage.getItem('price_id')
    var billingName = firstName + ' ' + lastName
    const data = {
        name: billingName,
        billingAddress: billingAddress,
        country: country,
        state: state,
        city: city,
        zip: zip,
        email: email
    }

    $.ajax({
        type: 'POST',
        async: false,
        crossDomain: true,
        url: '/subscription/create_customer/',
        data: data,
        dataType: "json",
        success: function (res) {
            createSubscription(res.data.id, priceId)
        },
        error: function (jqXHR, textStatus, err) {
            console.log(err)
        }
    })
}

function createSubscription(customerId, priceId) {
    var ownerName = document.getElementById('id_card_holder').value;
    var cardNum = document.getElementById('id_card_num').value;
    var month = document.getElementById('id_expiry_month').value;
    var year = document.getElementById('id_expiry_year').value;
    var cardCvv = document.getElementById('id_cvv').value;
    const data = {
        ownerName: ownerName,
        cardNumber: cardNum,
        month: month,
        year: year,
        cardCVV: cardCvv,
        customerId: customerId,
        priceId: priceId,
    }

    $.ajax({
        type: 'POST',
        async: false,
        crossDomain: true,
        url: '/subscription/create_subscription/',
        data: data,
        dataType: "json",
        success: function (res) {
            if (res.status === 1) {
                $('#submit').prop('disabled', false);
                sessionStorage.removeItem(product_id);
                sessionStorage.removeItem(price_id);
            } else {
                $('#submit').prop('disabled', true);
                window.location = baseUrl + '/subscription/payment_failed/'
            }
        },
        error: function (jqXHR, textStatus, err) {
            console.log(err)
        }
    })
}

function cancelSubscription(id) {
    const data = {
        'subscriptionId': id
    }
    $.ajax({
        type: 'POST',
        async: false,
        crossDomain: true,
        url: '/subscription/delete_subscription/',
        data: data,
        dataType: "json",
        success: function (res) {
            document.getElementById("status").innerHTML = res.data.status;
            document.getElementById("status").innerHTML = 'Subscription Status: canceled';
        },
        error: function (jqXHR, textStatus, err) {
            console.log(err)
        }
    })
}