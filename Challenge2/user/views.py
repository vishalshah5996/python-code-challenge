from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django_registration.backends.one_step.views import RegistrationView
from djstripe.models import Product, Plan, Subscription, Card

from user.forms import UserRegistrationForm
from user.models import User


@method_decorator(csrf_exempt, name='dispatch')
class UserRegisterView(RegistrationView):
    form_class = UserRegistrationForm
    template_name = 'users/account-detail-register.html'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('user_details')
        else:
            form = self.form_class()
            product = Product.objects.all().order_by('-djstripe_id')
            plan = Plan.objects.all()
            return render(request, self.template_name, {'form': form, 'product': product, 'plan': plan})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.subscription_id = request.session.get('subscription_id')
            request.session['subscription_id'] = None
            user.save()
            return redirect('payment_success')

        return render(request, self.template_name, {'form': form})


class UserDetailsView(TemplateView):
    template_name = 'user/index.html'

    def get(self, request, *args, **kwargs):
        user = User.objects.get(username=request.user.username)
        if user.is_superuser:
            subscription = None
        else:
            subscription = Subscription.objects.filter(id=user.subscription_id).order_by("-djstripe_id")
            for sub in subscription:
                subscription = sub
        return render(request, self.template_name, context={'user': user, 'subscription': subscription})


class SubscriptionRestrictedView(TemplateView):
    template_name = 'user/stripe_account_detail.html'

    def get(self, request, *args, **kwargs):
        user = User.objects.get(username=request.user.username)
        if user.is_superuser:
            subscription = None
            customer = None
            card_details = None
        else:
            subscription = Subscription.objects.filter(id=user.subscription_id)
            for sub in subscription:
                if sub.status == 'trialing' or sub.status == 'active':
                    subscription = sub
                    customer = sub.customer
                    card_details = Card.objects.filter(id=customer.default_payment_method.id)
                    card_details = [
                        {'last': card_detail.last4, 'name': card_detail.name, 'exp_month': card_detail.exp_month,
                         'exp_year': card_detail.exp_year, 'id': card_detail.id} for card_detail in card_details]
                    break
                else:
                    subscription = None
                    customer = None
                    card_details = None
        return render(request, self.template_name,
                      context={'user': user, 'subscription': subscription, 'customer': customer,
                               'card_details': card_details})
