from django.urls import path

from user.views import UserDetailsView, SubscriptionRestrictedView

urlpatterns = [
    path('account_details/', UserDetailsView.as_view(), name='user_details'),
    path('subscription_details/', SubscriptionRestrictedView.as_view(), name='subscription_details'),
]
