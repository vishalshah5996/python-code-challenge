let baseUrl = window.location.origin;

function storeValue(product_id, price_id) {
    console.log(product_id)
    sessionStorage.setItem("product_id", product_id);
    sessionStorage.setItem("price_id", price_id);
    window.location = baseUrl + '/accounts/register/'
}

function createCustomer() {
    // var paymentMethodId = paymentMethodId
    $('#submit').prop('disabled', true);
    document.getElementById('submit').removeAttribute("onclick");
    var billingName = document.getElementById('id_billing_name').value;
    var billingAddress = document.getElementById('id_billing_address').value;
    var country = document.getElementById('id_country').value;
    var state = document.getElementById('id_state').value;
    var city = document.getElementById('id_city').value;
    var zip = document.getElementById('id_postal_code').value;
    var email = document.getElementById('id_email').value;

    var priceId = sessionStorage.getItem('price_id')
    const data = {
        name: billingName,
        billingAddress: billingAddress,
        country: country,
        state: state,
        city: city,
        zip: zip,
        email: email,
        // paymentMethodId: paymentMethodId
    }

    console.log(data)
    $.ajax({
        type: 'POST',
        async: false,
        crossDomain: true,
        url: '/subscription/create_customer/',
        data: data,
        dataType: "json",
        success: function (res) {
            console.log(res.data.id);
            createSubscription(res.data.id, priceId)
        },
        error: function (jqXHR, textStatus, err) {
            console.log(err)
        }
    })
}

function createSubscription(customerId, priceId) {
    var ownerName = document.getElementById('id_card_holder').value;
    var cardNum = document.getElementById('id_card_num').value;
    var month = document.getElementById('id_expiry_month').value;
    var year = document.getElementById('id_expiry_year').value;
    var cardCvv = document.getElementById('id_cvv').value;
    const data = {
        ownerName: ownerName,
        cardNumber: cardNum,
        month: month,
        year: year,
        cardCVV: cardCvv,
        customerId: customerId,
        // paymentMethodId: paymentMethodId,
        priceId: priceId,
    }
    console.log(data)
    $.ajax({
        type: 'POST',
        async: false,
        crossDomain: true,
        url: '/subscription/create_subscription/',
        data: data,
        dataType: "json",
        success: function(res) {
            // createUser(customerId);
            // document.getElementById("signup-form").reset();
            // document.getElementById("paymentMethod-form").reset();
            console.log('res', res);
            if (res.status === 1) {
                console.log(res)
                $('#submit').prop('disabled', false);
            } else {
                $('#submit').prop('disabled', true);
                window.location = baseUrl + '/subscription/payment_failed/'
            }
        },
        error: function(jqXHR, textStatus, err) {
            console.log(err)
        }
    })
}
