from django.http import JsonResponse

"""
    This module contains class to prepare a proper server response and return it.
"""


class ApiResponse:
    """
        This is a class for constructing a common server response.

        Attributes:
            status (int): Status of api 0 or 1.
            message (int): message to be sent with response.
            data (dict): Data to be sent in api if not specified returns empty dict.
            http_status (HTTP_STATUS): This the http status sent with api if not specified explicitly restframework will
                                        handle it.
        """

    def __init__(self, status: int, message: str, data: dict = None, http_status=None):
        """

        :param status:  Status to be sent in response.
        :param message: message to be sent with response.
        :param data:    Data to be sent in api, if not specified returns empty dict
        :param http_status: HTTP_STATUS of api, if not specified explicitly then handled by restframework
        """
        self.response = dict()

        self.message = message
        self.status = status
        self.http_status = http_status

        if data:
            self.data = data
        else:
            self.data = None

    def create_response(self):
        """
        Creates a Response class object and returns it.

        :py:class:: `rest_framework.response.Response`
        :return: Returns the Response of rest_framework api.
        """
        self.response['status'] = self.status
        self.response['message'] = self.message
        self.response['data'] = self.data

        if self.http_status:
            return JsonResponse(self.response, status=self.http_status)
        else:
            return JsonResponse(self.response)


def get_error_message(serializer):
    errors = list(serializer.errors.values())
    error_keys = list(serializer.errors.keys())
    error_message = errors[0][0]
    return error_message
