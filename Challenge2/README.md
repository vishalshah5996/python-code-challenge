# Aben Premium

- Create and activate virtual environment 
```
python3 -m venv venv
source venv/bin/activate
```
- Install requirements
```
pip install -r requirements.txt
```
- Create postgres database and set environment variables shown below.
    - DBNAME: Your database name
    - DBUSER: your postgres username
    - DBPASS: your postgres password
    
- Create and set strip keys to environment variable 
    - STRIPE_TEST_PUBLIC_KEY: Your stripe public key
    - STRIPE_TEST_SECRET_KEY: Your stripe secret key
    - DJSTRIPE_WEBHOOK_SECRET: Your stripe webhook secret
    
- After creating database and setting environment variables run below commands.
```
python manage.py makemigrations
python manage.py migrate
python manage.py djstripe_sync_plans_from_stripe
```

- Run the server
```
python manage.py runserver
```

- Check here: http://127.0.0.1:8000/
