from django.urls import path

from subscription.views import CreateCustomer, CreateSubscription, PaymentFailedView, PaymentSuccessView, \
    DeleteSubscription

urlpatterns = [
    path('create_customer/', CreateCustomer.as_view(), name='create-customer'),
    path('create_subscription/', CreateSubscription.as_view(), name='create-subscription'),
    path('payment_success/', PaymentSuccessView.as_view(), name='payment_success'),
    path('payment_failed/', PaymentFailedView.as_view(), name='payment-fail'),
    path('delete_subscription/', DeleteSubscription.as_view(), name='delete_subscription'),
]
