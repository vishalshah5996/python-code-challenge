from datetime import datetime, timedelta

import stripe
from django.conf import settings
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, View
from djstripe.models import Product, Plan, PaymentMethod, Subscription, Invoice, Customer

import constants
from response_utils import ApiResponse

stripe.api_key = settings.STRIPE_TEST_SECRET_KEY


class PricingPage(TemplateView):
    """
    Display product pricing details
    """
    template_name = 'subscription/pricing.html'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('user_details')
        else:
            product = Product.objects.all().order_by('-djstripe_id')
            plan = Plan.objects.all()
            return render(request, self.template_name, context={'products': product, 'plans': plan})


@method_decorator(csrf_exempt, name='dispatch')
class CreateCustomer(View):
    def post(self, request):
        """
        Creates new customer
        """
        email = request.POST.get('email')
        name = request.POST.get('name')
        line1 = request.POST.get('billingAddress')
        country = request.POST.get('country')
        state = request.POST.get('state')
        city = request.POST.get('city')
        postal_code = request.POST.get('zip')
        try:
            customer = stripe.Customer.create(
                email=email,
                name=name,
                address={
                    'line1': line1,
                    'country': country,
                    'state': state,
                    'city': city,
                    'postal_code': postal_code
                }
            )
            return ApiResponse(status=1, message=constants.STRIPE_CUSTOMER_SUCCESS,
                               data=customer).create_response()

        except Exception as e:
            print(e)
            return ApiResponse(status=0, message=constants.STRIPE_CUSTOMER_FAILED,
                               data=None).create_response()


@method_decorator(csrf_exempt, name='dispatch')
class CreateSubscription(View):
    def post(self, request):
        """
        Creates new subscription
        """
        customerId = request.POST.get('customerId')
        priceId = request.POST.get('priceId')

        try:

            card_token = stripe.Token.create(
                card={
                    "number": request.POST.get('cardNumber'),
                    "exp_month": request.POST.get('month'),
                    "exp_year": request.POST.get('year'),
                    "cvc": request.POST.get('cardCVV'),
                    "name": request.POST.get('ownerName'),
                }
            )

            price_data = stripe.Plan.retrieve(priceId)

            # Creates source for customer using token of card object
            card_source = stripe.Customer.create_source(
                customerId,
                source=card_token,
            )

            # Update customer's default payment method
            stripe.Customer.modify(
                customerId,
                invoice_settings={
                    'default_payment_method': card_source.id,
                }
            )

            # Creates subscription for trial period
            subscription = stripe.Subscription.create(
                customer=customerId,
                items=[
                    {
                        'price': priceId
                    }
                ],
                trial_end=datetime.now() + timedelta(days=price_data.trial_period_days),
                expand=['latest_invoice.payment_intent'],
            )

            paymentMethod = stripe.PaymentMethod.retrieve(card_source.id)

            if subscription.status == "trialing":

                # Syncs user's stripe data with database
                retrieve_customer = stripe.Customer.retrieve(customerId)

                PaymentMethod.sync_from_stripe_data(paymentMethod)
                Subscription.sync_from_stripe_data(subscription)

                Invoice.sync_from_stripe_data(subscription.latest_invoice)

                Customer.sync_from_stripe_data(retrieve_customer)
                request.session['subscription_id'] = subscription.id

                return ApiResponse(status=1, message=constants.SUBSCRIPTION_CREATED_SUCCESS,
                                   data=subscription).create_response()

            elif subscription.status == "incomplete":
                # Deletes customer from stripe if payment status for subscription is incomplete
                stripe.Customer.delete(customerId)
                return ApiResponse(status=0, message=constants.SUBSCRIPTION_CREATED_FAILED,
                                   data=None).create_response()

        except Exception as e:
            print(e)
            return ApiResponse(status=0, message=constants.SUBSCRIPTION_CREATED_FAILED,
                               data=None).create_response()


class PaymentSuccessView(TemplateView):
    template_name = 'subscription/payment_success.html'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('user_details')
        else:
            return render(request, self.template_name)


class PaymentFailedView(View):
    template_name = 'subscription/payment_failed.html'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('user_details')
        else:
            return render(request, self.template_name)


@method_decorator(csrf_exempt, name='dispatch')
class DeleteSubscription(View):
    def post(self, request):
        """
        This function cancels user's existing subscription schedules (If any) and subscription
        """
        subscriptionId = request.POST.get('subscriptionId')
        try:
            subscription_status = stripe.Subscription.retrieve(subscriptionId)
            stripe.Subscription.delete(
                subscriptionId
            )
            canceled_subscription = stripe.Subscription.retrieve(subscriptionId)
            stripe_subscription = Subscription.sync_from_stripe_data(
                canceled_subscription
            )

            return ApiResponse(status=1, message=constants.SUBSCRIPTION_DELETED_SUCCESS,
                               data=canceled_subscription.status).create_response()

        except Exception as e:
            print(e)
            return ApiResponse(status=0, message=constants.SUBSCRIPTION_DELETE_FAILED,
                               data=None).create_response()
