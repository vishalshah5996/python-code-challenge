def identify_router(input_string_list):
    
    if len(input_string_list) <= 2:
        return ",".join(input_string_list)
    else:
        graph = dict()
        for index, node in enumerate(input_string_list,1):
            if index != len(input_string_list):
                if node in graph.keys():
                    graph[node].append(input_string_list[index])
                else:
                    graph[node] = [input_string_list[index]]
        
        print("Graph generated from input string is :", graph)
        
        connections_dict = dict()
        for key, value in graph.items():
            outbounds = len(value)
            inbounds = sum([1 for val in graph.values() if key in val])
            connections_dict[key] = outbounds + inbounds
        
        max_connections = max(connections_dict.values())
        	    
        return ",".join([k for k,v in connections_dict.items() if v == max_connections])


if __name__ == '__main__':
    print("Enter string represented as graph.\nFor Example: 1->2->3->5->2->1\nNote: Please enter string without any spaces\n")

    input_string = input("Enter string : ")
    input_string_list = input_string.split('->')

    print("\nNode with maximum connections are :", identify_router(input_string_list))

    
# time complexity = 3n + 2n*n
        

