import itertools


def compress(string):
    return ''.join(letter + str(len(list(group))) for letter, group in itertools.groupby(string))


if __name__ == '__main__':
    input_string = input("Enter the string: ")
    compress_string = compress(input_string)
    print(input_string if len(input_string) <= len(compress_string) else compress_string)


# time complexity : n + nlog(n)

# join = n
# string groupby = nlog(n)
